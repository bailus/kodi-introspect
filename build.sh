git config --global user.email "sam@bailey.geek.nz"
git config --global user.name "Bitbucket Pipelines"

git clone -b staging https://$USERNAME:$PASSWORD@bitbucket.org/bailus/kodi-addons.git
cd kodi-addons

rm -r webinterface.introspect
mv ../webinterface.introspect ./

git add .
git commit -m "Build using Bitbucket Pipelines"
git push
